from django.db import models
from operations.models import Hunter, Operation


class Message(models.Model):
    text = models.CharField(max_length=1024, blank=False, default='msg')
    sender = models.ForeignKey(Hunter, on_delete=models.CASCADE)
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.sender.name}: {str(self.created)}'
