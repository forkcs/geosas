from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest, HttpResponseNotFound

from .models import Message
from operations.models import Hunter, Operation, token_is_valid


def message(request):

    if request.method == 'POST':
        data = request.POST
        token = data.get('token')
        text = data.get('text')
        hunter_id = data.get('hunter_id')

        if not (token and text and hunter_id):
            return HttpResponseBadRequest()
        if not token_is_valid(token):
            return HttpResponseNotFound()

        Message.objects.create(text=text, sender=Hunter.objects.get(local_id=hunter_id, operation__token=token),
                               operation=Operation.objects.get(token=token))
        response = HttpResponse(status=200)
        if request.is_ajax():
            response['Access-Control-Allow-Origin'] = '*'
        return response

    elif request.method == 'GET':
        token = request.GET.get('token')

        if not token:
            return HttpResponseBadRequest()
        if not token_is_valid(token):
            return HttpResponseNotFound()

        messages = Message.objects.filter(operation__token=token)
        response = JsonResponse({'messages': [{'name': msg.sender.name, 'text': msg.text} for msg in messages]})
        if request.is_ajax():
            response['Access-Control-Allow-Origin'] = '*'
        return response
