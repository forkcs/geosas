from django.contrib import admin
from .models import Operation, Hunter

admin.site.register(Operation)
admin.site.register(Hunter)
