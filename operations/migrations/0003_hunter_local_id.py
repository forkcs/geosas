# Generated by Django 2.1.3 on 2018-11-25 10:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('operations', '0002_auto_20181125_0923'),
    ]

    operations = [
        migrations.AddField(
            model_name='hunter',
            name='local_id',
            field=models.IntegerField(default=None),
        ),
    ]
