# Generated by Django 2.1.3 on 2018-11-25 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('operations', '0004_remove_hunter_local_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='hunter',
            name='local_id',
            field=models.IntegerField(default=None),
        ),
    ]
