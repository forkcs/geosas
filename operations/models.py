from random import choice
from django.db import models


def create_token():
    abc = 'abcdefghijklmnopqrstuvwxyz0123456789'
    return choice(abc) + choice(abc) + choice(abc) + choice(abc) + choice(abc)


class Operation(models.Model):

    @classmethod
    def create(cls):
        operation = cls(token=create_token())
        return operation

    token = models.CharField(max_length=5, unique=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.token


class Hunter(models.Model):
    hunter_id = models.IntegerField(default=1)
    name = models.CharField(max_length=32, blank=True, default='Hunter')
    operation = models.ForeignKey(Operation, default=None, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


def token_is_valid(token):
    if len(token) != 5:
        return False
    if not Operation.objects.filter(token=token).exists():
        return False
    return True
