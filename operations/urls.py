from django.urls import path
from .views import operation, hunter


urlpatterns = [
    path('operation/', operation),
    path('hunter/', hunter),
]
