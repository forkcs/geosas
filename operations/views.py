import json
from django.http import HttpResponseBadRequest, HttpResponseNotFound, JsonResponse
from django.db import IntegrityError
from .models import Hunter, Operation, token_is_valid


def operation(request):

    if request.method != 'POST':
        return HttpResponseBadRequest()

    while True:
        try:
            new_operation = Operation.create()
            new_operation.save()
        except IntegrityError:
            pass
        else:
            break

    response = JsonResponse({'token': new_operation.token}, status=201)
    if request.is_ajax():
        response['Access-Control-Allow-Origin'] = '*'
    return response


def hunter(request):

    if request.method != 'POST':
        return HttpResponseBadRequest()

    data = json.loads(request.body)

    token = data.get('token')
    name = data.get('name')

    if not token:
        return HttpResponseBadRequest()
    if not token_is_valid(token):
        return HttpResponseNotFound()

    ids = [_hunter.hunter_id for _hunter in Hunter.objects.filter(operation__token=token)]
    hunter_id = max(ids) + 1 if ids else 1
    Hunter.objects.create(operation=Operation.objects.get(token=token),
                          hunter_id=hunter_id, name=name or 'Hunter '+str(hunter_id))
    response = JsonResponse({'hunter_id': hunter_id}, status=201)
    if request.is_ajax():
        response['Access-Control-Allow-Origin'] = '*'
    return response
