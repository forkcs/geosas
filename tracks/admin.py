from django.contrib import admin
from .models import Point, Marker

admin.site.register(Point)
admin.site.register(Marker)
