from django.db import models
from operations.models import Hunter, Operation


class Point(models.Model):
    latitude = models.DecimalField(max_digits=8, decimal_places=6, default=None)
    longitude = models.DecimalField(max_digits=8, decimal_places=6, default=None)
    timestamp = models.DateTimeField()
    hunter = models.ForeignKey(Hunter, on_delete=models.CASCADE, default=None, blank=False)

    def __str__(self):
        return str(self.timestamp)


class Marker(models.Model):
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE, blank=False, default=None)
    latitude = models.DecimalField(max_digits=8, decimal_places=6, default=None)
    longitude = models.DecimalField(max_digits=8, decimal_places=6, default=None)
    description = models.CharField(max_length=512, default='', blank=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.description[:150]+'...'
