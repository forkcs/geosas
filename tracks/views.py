from datetime import datetime
import json

from django.http import HttpResponse, JsonResponse, HttpResponseNotFound, HttpResponseBadRequest

from .models import Point, Marker
from operations.models import Hunter, Operation, token_is_valid


def track(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        hunter_id = data['hunter_id']
        token = data['token']
        geodata = data['geo']

        if not (hunter_id and token and geodata):
            return HttpResponseBadRequest()
        if not token_is_valid(token):
            return HttpResponseNotFound()

        for point in geodata:
            Point.objects.create(hunter=Hunter.objects.get(hunter_id=hunter_id, operation__token=token),
                                 latitude=point['y'], longitude=point['x'], timestamp=datetime(*point['t']))
        response = HttpResponse(status=201)
        if request.is_ajax():
            response['Access-Control-Allow-Origin'] = '*'
        return response

    elif request.method == 'GET':
        hunter_id = request.GET.get('hunter_id')
        token = request.GET.get('token')

        if not (hunter_id and token):
            return HttpResponseBadRequest()
        if not token_is_valid(token):
            return HttpResponseNotFound()

        if hunter_id == '0':
            ids = [hunter.hunter_id for hunter in Hunter.objects.filter(operation__token=token)]
        else:
            ids = [hunter_id]

        tracks = []
        for hid in ids:
            hunter = Hunter.objects.get(hunter_id=hid, operation__token=token)
            name = hunter.name
            geo = []
            for point in Point.objects.filter(hunter=hunter):
                x = point.longitude
                y = point.latitude
                t = [*point.timestamp.timetuple()[:5]]
                geo.append({'x': float(x), 'y': float(y), 't': t})
            tracks.append({'name': name, 'hunter_id': hid, 'geo': geo})
        data = {'tracks': tracks}
        response = JsonResponse(data)
        if request.is_ajax():
            response['Access-Control-Allow-Origin'] = '*'
        return response


def marker(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body.decode('utf-8'))
        except json.decoder.JSONDecodeError:
            return HttpResponseBadRequest()

        token = data['token']
        geo = data['geo']
        description = data['description']

        if not (token and geo and description):
            return HttpResponseBadRequest()
        if not token_is_valid(token):
            return HttpResponseNotFound()

        Marker.objects.create(description=description, latitude=geo['y'], longitude=geo['x'],
                              operation=Operation.objects.get(token=token))
        response = HttpResponse(status=201)
        if request.is_ajax():
            response['Access-Control-Allow-Origin'] = '*'
        return response

    elif request.method == 'GET':
        token = request.GET.get('token')

        if not token:
            return HttpResponseBadRequest()
        if not token_is_valid(token):
            return HttpResponseNotFound()

        markers = []
        for marker_ in Operation.objects.get(token=token).marker_set.all():
            x = marker_.longitude
            y = marker_.latitude
            markers.append({'x': float(x), 'y': float(y)})
        data = {'markers': markers}
        response = JsonResponse(data)
        if request.is_ajax():
            response['Access-Control-Allow-Origin'] = '*'
        return response
